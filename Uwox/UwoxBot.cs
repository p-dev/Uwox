﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;
using Newtonsoft.Json;

namespace Uwox
{
    public static class UwoxBot
    {
        public static DiscordSocketClient Client;

        public static Dictionary<string, ulong> Channels;

        public static void ReadyInit()
        {
            Channels = new Dictionary<string, ulong>();
            foreach (var channel in Client.GetGuild(355635520283344897ul).Channels)
            {
                Channels.Add(channel.Name, channel.Id);
                Console.WriteLine($"Added channel {channel.Name}:{Channels[channel.Name]} in list.");
            }
        }

        public static async Task RunAsync()
        {
            await Client.StartAsync();

            await Task.Delay(-1);
        }
    }
}