﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;


namespace Uwox
{
    internal class Program
    {
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;
        
        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private async Task MainAsync()
        {
            File.CreateText("./Logs/console-log.txt");
            
            // Client
            _client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                LogLevel = LogSeverity.Debug,
            });
            UwoxBot.Client = _client;
            
            // Commands
            _commands = new CommandService(new CommandServiceConfig()
            {
                CaseSensitiveCommands = true,
                DefaultRunMode = RunMode.Async,
                LogLevel = LogSeverity.Debug
            });

            // Services
            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();
            
            // Event subscriptions
            _client.Log += ConsoleLog;
            _client.Ready += BotReadyAsync;
            _client.MessageReceived += EmojiChoicer.EmojiChoicer.MessageReceivedInChoiceChannelAsync;
            _client.MessageReceived += GlobalObservers.MessageReceivedAsync;
            
            await RegisterCommandsAsync();
            
            const string token = "NDYzNzg3MTEzMzM5NTUxNzU3.Dh1gAg.IR9PP2R7KZ5kXsJJ8fOEm8gSdnI"; // JsonConvert.DeserializeObject<string>(File.ReadAllText("Data/bot-config.json"));
            await _client.LoginAsync(TokenType.Bot, token);

            await UwoxBot.RunAsync();
        }

        private Task BotReadyAsync()
        {
            UwoxBot.ReadyInit();
            return Task.CompletedTask;
        }

        private Task ConsoleLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            
            File.AppendAllText("./Logs/console-log.txt", arg.ToString() + "\n");
            
            return Task.CompletedTask;
        }

        private async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            
            if (message is null || message.Author.IsBot) return;

            var argPos = 0;

            if (message.HasStringPrefix("!", ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var context = new SocketCommandContext(_client, message);

                var result = await _commands.ExecuteAsync(context, argPos, _services);

                if (!result.IsSuccess)
                    Console.WriteLine(result.ErrorReason);
            }
        }
    }
}