﻿using System;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Xml;

namespace Uwox
{
    public class Table
    {
        public const char BorderH  = '─';
        public const char BorderV  = '│';
        // ReSharper disable InconsistentNaming
        public const char BorderDR = '┌';
        public const char BorderDL = '┐';
        public const char BorderUR = '└';
        public const char BorderUL = '┘';
        public const char BorderVR = '├';
        public const char BorderVL = '┤';
        public const char BorderHD = '┬';
        public const char BorderHU = '┴';
        public const char BorderVH = '┼';
        // ReSharper restore InconsistentNaming

        public int Height { get; }
        public int Width { get; }
        
        public string TableString { get; private set; }
        public string[,] TableObjects { get; set; }
        
        public Table(int h, int w)
        {
            Height = h;
            Width = w;
            
            TableObjects = new string[Height, Width];
        }

        private void BuildTable()
        {
            var maxObjLengths = new int[Width];
            for (var j = 0; j < Width; j++)
            {
                maxObjLengths[j] = TableObjects[0, j].Length;
                for (var i = 1; i < Height; i++)
                {
                    if (maxObjLengths[j] < TableObjects[i, j].Length) maxObjLengths[j] = TableObjects[j, i].Length;
                }
            }
            
            var tableChars = new char[Height * 2 + 1, maxObjLengths.Sum() + Width * 2 + Width + 1];
            for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                    tableChars[i, j] = ' ';
            
            // Build Char Matrix
            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    if (i == 0)
                    {
                        /* o..
                           ...
                           ... */
                        if (j == 0)
                        {
                            for (var iStr = 0; i < 3; i++)
                            {
                                for (var jStr = 0; j < maxObjLengths[j] + 2 + 2; jStr++)
                                {
                                    if (iStr == 0)
                                    {
                                        if (jStr == 0)
                                        {
                                            tableChars[i + iStr, j + jStr] = BorderDR;
                                        }

                                        if (jStr == Width - 1)
                                        {
                                            tableChars[i + iStr, j + jStr] = BorderHD;
                                        }
                                        else
                                        {
                                            tableChars[i + iStr, j + jStr] = BorderV;
                                        }
                                    }
                                    else if (iStr == 3 - 1)
                                    {
                                        if (jStr == 0)
                                        {
                                            tableChars[i + iStr, j + jStr] = BorderVR;
                                        }
                                        else if (jStr == Width - 1)
                                        {
                                            tableChars[i + iStr, j + jStr] = BorderVH;
                                        }
                                        else
                                        {
                                            tableChars[i + iStr, j + jStr] = BorderV;
                                        }
                                    }
                                    else if (jStr == 0)
                                    {
                                        tableChars[i + iStr, j + jStr] = BorderV;
                                    }
                                    else if (jStr == Width - 1)
                                    {
                                        tableChars[i + iStr, j + jStr] = BorderV;
                                    }
                                    else
                                    {
                                        for (var iObj = 0; iObj < maxObjLengths[j]; iObj++)
                                        {
                                            tableChars[i + iStr , j + jStr + 1] = TableObjects[i, j][iObj];
                                        }
                                    }
                                }
                            }
                        }
                        /* ..o
                           ...
                           ... */
                        else if (j == Width - 1)
                        {
                            
                        }
                        /* .o.
                           ...
                           ... */
                        else
                        {
                            
                        }
                    }
                    else if (j == 0)
                    {
                        /* ...
                           ...
                           o.. */
                        if (i == Height - 1)
                        {
                            
                        }
                        /* ...
                           o..
                           ... */
                        else
                        {
                            
                        }
                    }
                    else if (i == Height - 1)
                    {
                        /* ...
                           ...
                           ..o */
                        if (j == Width - 1)
                        {
                            
                        }
                        /* ...
                           ...
                           .o. */
                        else
                        {
                            
                        }
                    }
                    /* ...
                       ..o
                       ... */
                    else if (j == Width - 1)
                    {
                        
                    }
                    /* ...
                       .o.
                       ... */
                    else
                    {
                        
                    }
                }
            }
            
            // Chars Matrix to One String
            TableString = "";
            for (var i = 0; i < Height * 2 + 1; i++)
            {
                for (var j = 0; j < maxObjLengths.Sum() + Width * 2 + Width + 1; j++)
                {
                    TableString += tableChars[i, j];
                }
                TableString += '\n';
            }
        }
        
    }
}