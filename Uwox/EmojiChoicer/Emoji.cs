﻿namespace Uwox.EmojiChoicer
{
    public class Emoji
    {
        public string Name { get; set; }
        
        public string Url { get; set; }
    }
}