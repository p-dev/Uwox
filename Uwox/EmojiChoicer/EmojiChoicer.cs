﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

namespace Uwox.EmojiChoicer
{
    public static class EmojiChoicer
    {   
        private static List<Emoji> _emojis = new List<Emoji>();

        public static async Task MessageReceivedInChoiceChannelAsync(SocketMessage arg)
        {
//            if (arg.Channel.Id != UwoxBot.Channels?["emoji-choice"] || arg.Author.IsBot) return;
            return;
            
            if (Regex.IsMatch(arg.Content, ":*:") &&
                arg.Attachments.Count == 1)
            {
                _emojis.Append(new Emoji()
                {
                    Name = arg.Content,
                    Url = arg.Attachments.First().Url,
                });

                var embed = new EmbedBuilder()
                    .WithColor(Color.Gold)
                    .WithTitle("New emoji registered.")
                    .WithDescription("Waitng for your points.")
                    .Build();
                await arg.Channel.SendMessageAsync("", false, embed);
            }
            else
            {
                var embed = new EmbedBuilder()
                    .WithColor(Color.Gold)
                    .WithTitle("Not correct message format.")
                    .WithDescription(":<emoji-name>:" + "\n" +
                                     "<emoji-image>")
                    .Build();
                var errorMessage = await arg.Channel.SendMessageAsync("", false, embed);

                await Task.Delay(4000);

                await arg.Channel.DeleteMessagesAsync(new IMessage[]{arg, errorMessage});
            }
        }
    }
}