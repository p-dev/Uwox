﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Uwox.Modules
{
    public class Ping : ModuleBase<SocketCommandContext>
    {
        [Command("say_to")]
        [Name("say_to <id> <text...>")]
        public async Task SayToCommandAsync(ulong id, RemainderAttribute text)
        {
            await ((IMessageChannel) Context.Client.GetChannel(id)).SendMessageAsync(text.ToString());
        }
        
        [Command("clean")]
        [Name("clean <count> (<show_mesage>)")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task CleanCommandAsync(int count, bool showMessage = true)
        {
            var messages = (await Context.Channel.GetMessagesAsync(count + 1).Flatten()).ToList();
            var findedMessagesCount = messages.Count;
            await Context.Channel.DeleteMessagesAsync(messages);
            
            if (showMessage)
            {
                const int delay = 3000;

                var embed = new EmbedBuilder()
                    .WithColor(Color.DarkPurple)
                    .WithTitle("Clean messages.")
                    .WithDescription("Deleted " + (findedMessagesCount - 1) + ((findedMessagesCount - 1 > 1) ? " messages." : " message."))
                    .Build();

                var deleteMessage = await ReplyAsync("", false, embed);
                await Task.Delay(delay);
                await deleteMessage.DeleteAsync();
            }
        }

        [Command("dice")]
        public async Task DiceCommandAsync(uint d, uint count = 1)
        {
            var dices = new long[count];
            
            for (var i = 0; i < count; i++)
            {
                dices[i] = new Random().Next(1, (int) d + 1);
            }

            var diceMax = dices.Max();
            var diceMin = dices.Min();
            
            var dicesString = "";
            foreach (var dice in dices)
            {
                if (dice == diceMax) dicesString += "**";
                if (dice == diceMin) dicesString += "__";
                
                dicesString += dice;
                
                if (dice == diceMin) dicesString += "__";
                if (dice == diceMax) dicesString += "**";

                dicesString += " ";
            }
            
            var embed = new EmbedBuilder()
                .WithColor(Color.DarkPurple)
                .WithTitle($"Dice roll.")
                .WithDescription($"{Context.User.Mention} rolled {count} D{d}.")
                .AddField("Rolled:", dicesString)
                .AddInlineField("Summary:", dices.Sum())
                .Build();

            await ReplyAsync("", false, embed);
        }

        [Command("channels")]
        public async Task ChannelsWorkCommandAsync(string action, ulong id = 0)
        {
            var channelName = (Context.Client.GetChannel(id) as IMessageChannel)?.Name;

            Embed embed;
            
            switch (action)
            {
                case "add":
                    UwoxBot.Channels.Add(channelName, id);

                    embed = new EmbedBuilder()
                        .WithColor(Color.Green)
                        .WithTitle("Added channel to list")
                        .WithDescription($"{channelName}:{id}")
                        .Build();
                    break;
                
                case "del":
                    UwoxBot.Channels.Remove(channelName);
                    
                    embed = new EmbedBuilder()
                        .WithColor(Color.DarkOrange)
                        .WithTitle("Deleted channel from list")
                        .WithDescription($"{channelName}:{id}")
                        .Build();
                    break;
                
                case "list":
                    var channelsStrList = "";

                    foreach (var channel in UwoxBot.Channels)
                    {
                        channelsStrList += $"{channel.Key}:{channel.Value}\n";
                    }
                    
                    embed = new EmbedBuilder()
                        .WithColor(Color.DarkPurple)
                        .WithTitle("Channel list")
                        .WithDescription(channelsStrList)
                        .Build();
                    break;
                
                default:
                    embed = new EmbedBuilder()
                        .WithColor(Color.Red)
                        .WithTitle("Not correct command format")
                        .WithDescription("<action> don't found.")
                        .Build();
                    break;
            }

            await ReplyAsync("", false, embed);
        }
        
        [Command("ping")]
        public async Task TestCommandAsync()
        {
            await ReplyAsync("pong!");
        }
    }
}